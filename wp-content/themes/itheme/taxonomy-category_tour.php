<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
global $redux_theme;
get_header();
get_template_part("inc/menu");
get_search_form();
$cat_id = get_query_var('category_tour');
?>

<div class="col-xs-12 col-sm-12 nopadding template-theme">
    <div class="container">
        <div class="row">

    <?php
    get_sidebar();
    ?>
          <div class="col-xs-12 col-sm-12 col-md-9 main-box">


                <h1 class="title-block">
                    <?php
                    $term=get_term_by('slug',$cat_id,'category_tour');
                    echo $term->name;
                    ?>
                </h1>


              <?php
              the_archive_description( '<div class="col-xs-12 col-sm-12 col-md-12 nopadding taxonomy-description">', '</div>' );
              ?>

                <div class="row">

                    <?php
                    if(have_posts()):
                    while (have_posts()):
                    the_post();
                    ?>
                    <div class="col-xs-12 col-sm-12 nopadding items-loop-tour">

                        <div class="col-xs-5 col-sm-4 col-md-3 left">
                            <a href="<?php the_permalink(); ?>"><img src="<?php
                                $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                                echo $img[0];
                                ?>"> </a>
                        </div>
                        <div class="col-xs-7 col-sm-8 col-md-9 right">
                            <a href="<?php the_permalink(); ?>" class="header-title-tour"><?php the_title(); ?></a>
                            <div class="col-xs-12 col-sm-8 col-md-8 nopadding info-tour-loop">
                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Lịch trình:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour citylist">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-lichtrinh-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Thời gian:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Khởi hành:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Vận chuyển:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-transport-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Khách sạn :
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour box-star-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                                        $meta=(int) $meta;
                                        ?>
                                        <?php
                                        for($i=1;$i<=$meta;$i++):
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <?php
                                        endfor;
                                        ?>
                                    </div>
                                </div>


                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 nopadding text-right price-box-loop">
                                <p>Giá Tour</p>

                                <?php
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                    $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                }
                                else{
                                    if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                    }
                                }
                                if($price):
                                    $price=(float) $price;
                                    if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                        ?>
                                        <p><strong><del>
                                                    <?php
                                                    $int=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
                                                    echo number_format($int,0,'.','.');
                                                    ?> đ
                                                </del></strong></p>
                                        <?php
                                    endif;
                                    ?>
                                    <p class="price-tour-loop"><?=number_format($price,0,'.','.'); ?>  đ</p>
                                    <?php
                                endif;
                                ?>



                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <a href="<?php the_permalink(); ?>" class="linktour">Xem ngay</a>
                                </div>

                            </div>

                        </div>

                    </div>
                    <?php
                    endwhile;
                    endif;
                    wp_reset_query();
                    ?>


                </div>



              <div class="col-xs-12 col-sm-12 col-md-12 nopadding pagination"  >

                  <?php
                  global $wp_query;

                  $big = 999999999;

                  echo paginate_links( array(
                      'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                      'format' => '?paged=%#%',
                      'prev_text'    => __('« Mới hơn'),
                      'next_text'    => __('Tiếp theo »'),
                      'current' => max( 1, get_query_var('paged') ),
                      'total' => $wp_query->max_num_pages
                  ) );
                  ?>

              </div>



            </div>


        </div>
    </div>
</div>


<?php
get_footer();
?>
