<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
global $redux_theme;
get_header();
get_template_part("inc/menu");
get_search_form();

$termfirst = get_term_by( 'id', $redux_theme['opt-cattour'][0], 'category_tour' );
?>

<div class="col-xs-12 col-sm-12 nopadding templatebox box1" >
    <div class="container" >


        <div class="col-xs-12 col-sm-12 nopadding title-page">
            <i class="icon-tour"></i> <h3><?=$termfirst->name; ?></h3>
        </div>

        <div class="col-xs-12 col-sm-12 nopadding nomargin template1" >

            <div class="row">


                <?php
                $args = array( 'post_type' => 'tour', 'posts_per_page' => 2, 'category_tour' => $termfirst->slug);
                $wp=new WP_Query($args);
                if($wp->have_posts()):
                while ($wp->have_posts()):
                $wp->the_post();
                ?>
                <div class="col-xs-12 col-sm-6 col-md-6 items-template1" >

                    <div class="col-xs-5 col-sm-5 col-md-7 nopadding left" >
                        <a href="<?php the_permalink(); ?>" >
                      <?php 
                        if(get_post_meta(get_the_ID(),'meta-hot-tour',true) && get_post_meta(get_the_ID(),'meta-hot-tour',true)=="yes"){
                            ?>
                            <span class="icon-hot"></span>
                            <?php
                        }
                        ?>
                        <img src="<?php
                            $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                            echo $img[0];
                            ?>" /> </a>
                    </div>
                    <div class="col-xs-7 col-sm-7 col-md-5 nopadding right" >
                        <a href="<?php the_permalink(); ?>" class="link-tour" ><?php the_title(); ?></a>
                        <div class="col-xs-12 col-sm-12 nopadding info-tour" >

                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Thời gian
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                                    echo $meta;
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Khởi hành
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                                    echo $meta;
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Điểm khởi hành
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                  $term_th=get_the_terms(get_the_ID(),'diemdi');
                            echo $name_term=$term_th[0]->name;
                        
                                    ?>
                                </div>
                            </div>

                               <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Điểm đến
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $term_th=get_the_terms(get_the_ID(),'diemden');
                            echo $name_term=$term_th[0]->name;
                                    ?>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Vận chuyển
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-transport-tour',true);
                                    echo $meta;
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Khách sạn
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                                    $meta=(int) $meta;
                                    ?>
                                    <?php
                                    for($i=1;$i<=$meta;$i++):
                                    ?>
                                    <i class="fa fa-star"></i>
                                        <?php
                                        endfor;
                                        ?>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour tour-link-box">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding left" >
                                    
                                    <div class="col-xs-12 col-sm-12 col-md-12 nopadding boxithemect">
<?php
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                    $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                }
                                else{
                                    if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                    }
                                }
                                if($price):
                                    $price=(float) $price;
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                    ?>
                                    <p><strong><del>
                                                <?php
                                                $int=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
                                                echo number_format($int,0,'.','.');
                                                ?> đ
                                            </del></strong></p>
                                    <?php
                                        endif;
                                ?> 
                                <div class="col-xs-12 col-sm-12 col-md-12 nopadding <?php
                                if(!get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                    echo 'paddingtop13';
                                    endif;
                                 ?>">
Giá : <span class="price-tour"><?=number_format($price,0,'.','.'); ?> đ</span>
                                </div>
                                    <?php
                                endif;
                                ?>
                                </div>



                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding right">
                                    <a href="<?php the_permalink(); ?>" class="btnreadtour" >Xem ngay</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <?php
                endwhile;
                endif;
                wp_reset_postdata();
                ?>


            </div>

        </div>

    </div>
</div>

<div class="col-xs-12 col-sm-12 nopadding box-main" >
    <div class="container" >



        <?php
        $dem=0;
        $listcat1=$redux_theme['opt-cattour'];
        foreach ($listcat1 as $key=>$value):
        if($dem>0):
            $term=get_term_by('id',$value,'category_tour');
        ?>
        <div class="col-xs-12 col-sm-12 nopadding title-page">
            <i class="icon-tour"></i> <h3><?=$term->name; ?></h3>
        </div>


        <div class="row5">

            <div class="col-xs-12 col-sm-12 nopadding owl-carousel owl-theme owl-carousel-home">

                <?php

                $args = array( 'post_type' => 'tour', 'posts_per_page' =>8, 'category_tour' =>$term->slug);
                $wp=new WP_Query($args);
                if($wp->have_posts()):
                    while ($wp->have_posts()):
                        $wp->the_post();
                ?>
                <div class="col-xs-12 col-sm-12 nopadding item">
                    <div class="col-xs-12 col-sm-12 nopadding box-items">
                        <div class="col-xs-12 col-sm-12 nopadding text-center header-thumbnail" >
                            <a href="<?php the_permalink(); ?>" >
                           
                            <?php 
 if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
 	$saleprice=(float) get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
 $price=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
$textsale=($price-$saleprice)/$price;
$textsale=number_format($textsale*100,0);
                            ?>
                                <span class="icon-sale"><?=$textsale; ?>%</span>
                                <?php 
                                else:
                                    ?>

                                  <?php 
                        if(get_post_meta(get_the_ID(),'meta-hot-tour',true) && get_post_meta(get_the_ID(),'meta-hot-tour',true)=="yes"){
                            ?>
                            <span class="icon-hot"></span>
                            <?php
                        }
                        ?>

                                <?php
endif;
                                ?>
                                <img src="<?php
                                $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                                echo $img[0];
                                ?>" /> </a>
                        </div>
                        <div class="col-xs-12 col-sm-12 nopadding info-items-owl" >
                            <a href="<?php the_permalink(); ?>" class="link-info-items-owl" >
                                <?php the_title(); ?>
                            </a>

                            <div class="col-xs-12 col-sm-12 nopadding info-loop-items" >
                                <div class="col-xs-12 col-sm-12 nopadding" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        Thời gian :
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        Khởi hành :
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        Điểm khởi hành :
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                         <?php
                                  $term_th=get_the_terms(get_the_ID(),'diemdi');
                            echo $name_term=$term_th[0]->name;
                        
                                    ?>
                                    </div>
                                </div>

                                  <div class="col-xs-12 col-sm-12 nopadding" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        Điểm đến :
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                         <?php
                                  $term_th=get_the_terms(get_the_ID(),'diemden');
                            echo $name_term=$term_th[0]->name;
                                    ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        Vận chuyển :
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-transport-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        Khách sạn :
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                                        $meta=(int) $meta;
                                        ?>
                                        <?php
                                        for($i=1;$i<=$meta;$i++):
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <?php
                                        endfor;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding info-itemsrow" >
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding left" >



 <div class="col-xs-12 col-sm-12 col-md-12 nopadding boxithemect">                                   
<?php
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                    $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                }
                                else{
                                    if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                    }
                                }
                                if($price):
                                    $price=(float) $price;
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                    ?>
                                    <p><strong><del>
                                                <?php
                                                $int=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
                                                echo number_format($int,0,'.','.');
                                                ?> đ
                                            </del></strong></p>
                                    <?php
                                        endif;
                                ?> 
                                <div class="col-xs-12 col-sm-12 col-md-12 nopadding <?php
                                if(!get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                echo 'paddingtop13';
                                }
                                 ?>">
                                
Giá : <span class="price-tour"><?=number_format($price,0,'.','.'); ?> đ</span>
                                </div>
                                    <?php
                                endif;
                                ?>

                                </div>




                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding right text-right" >
                                        <a href="<?php the_permalink(); ?>" class="btnreadtour" >Xem ngay</a>
                                    </div>
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                        <?php
                        endwhile;
                        endif;
                        wp_reset_postdata();
                        ?>

            </div>

        </div>
        <?php
        endif;
        $dem++;
        endforeach;
        ?>



    </div>
</div>


<div class="col-xs-12 col-sm-12 nopadding templatebox box2" >
    <div class="container" >

        <div class="col-xs-12 col-sm-12 nopadding nomargin template1" >

            <div class="row">

                <?php
                foreach ($redux_theme['opt-cattour-list'] as $key=>$value):
                    $term=get_term_by('id',$value,'category_tour');
                ?>
                <div class="col-xs-6 col-sm-6 col-md-6 items-template1" >

                    <div class="col-xs-12 col-sm-12 header-tour" >
                        <a href="<?php
                        $link=get_term_link($term->slug,'category_tour');
                        echo $link;
                        ?>">
                            <?=$term->name;  ?>
                        </a>
                    </div>
                    <div class="col-xs-12 col-sm-12 nopadding box-items-template">
                        <?php
                        $args = array( 'post_type' => 'tour', 'posts_per_page' =>1, 'category_tour' =>$term->slug);
                        $wp=new WP_Query($args);
                        if($wp->have_posts()):
                            while($wp->have_posts()):
                                $wp->the_post();
                        ?>
                        <div class="col-xs-12 col-sm-12 nopadding title-tour-view" >
                            <a href="<?php the_permalink(); ?>" class="link-tour" ><?php the_title(); ?></a>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 nopadding left" >
                           
<div class="col-xs-12 col-sm-12 col-md-12 nopadding boxleft">
                            <a href="<?php the_permalink(); ?>" >
                            <img src="<?php
                                $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                                echo $img[0];
                                ?>" /> </a>
                                </div>
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7 nopadding right" >
                            <div class="col-xs-12 col-sm-12 nopadding info-tour" >
                                <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        Thời gian
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        Khởi hành
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        Điểm khởi hành
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        <?php
                                         $term_th=get_the_terms(get_the_ID(),'diemdi');
                            echo $name_term=$term_th[0]->name;
                                        ?>
                                    </div>
                                </div>

                                    <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        Điểm đến
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        <?php
                                         $term_th=get_the_terms(get_the_ID(),'diemden');
                            echo $name_term=$term_th[0]->name;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        Vận chuyển
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-transport-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                        Khách sạn
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                                        $meta=(int) $meta;
                                        ?>
                                        <?php
                                        for($i=1;$i<=$meta;$i++):
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <?php
                                        endfor;
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 nopadding items-info-tour tour-link-box">
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding left" >

                                    <div class="col-xs-12 col-sm-12 nopadding metapricetourtheme">
                                    






 <div class="col-xs-12 col-sm-12 col-md-12 nopadding boxithemect fontcttour">                                   
<?php
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                    $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                }
                                else{
                                    if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                    }
                                }
                                if($price):
                                    $price=(float) $price;
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                    ?>
                                    <p class="paddingtop3"><strong><del>
                                                <?php
                                                $int=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
                                                echo number_format($int,0,'.','.');
                                                ?> đ
                                            </del></strong></p>
                                    <?php
                                        endif;
                                ?> 
                                <div class="col-xs-12 col-sm-12 col-md-12 nopadding <?php
                                if(!get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                echo 'paddingtop13';
                                }
                                else{
                                    echo 'fontctprice';
                                }
                                 ?>">    
Giá : <span class="price-tour"><?=number_format($price,0,'.','.'); ?> đ</span>
                                </div>
                                    <?php
                                endif;
                                ?>

                                </div>





                                        </div>

                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding right">
                                        <a href="<?php the_permalink(); ?>" class="btnreadtour" >Xem ngay</a>
                                    </div>
                                </div>

                            </div>
                        </div>
                                <?php
                                endwhile;
                                endif;
                                wp_reset_postdata();
                                ?>

                    </div>

                    <div class="col-xs-12 col-sm-12 padding10 news-headlines" >
                        <div class="col-xs-12 col-sm-12 nopadding list-news-headlines" >

                            <?php
                            $dem=1;
                            $args = array( 'post_type' => 'tour', 'posts_per_page' =>6, 'category_tour' =>$term->slug);
                            $wp=new WP_Query($args);
                            if($wp->have_posts()):
                            while ($wp->have_posts()):
                            $wp->the_post();
                            if($dem>1):
                            ?>
                            <div class="col-xs-12 col-sm-12 col-md-12 nopadding items-list-news-tour">
                                <div class="col-xs-7 col-sm-7 col-md-7 nopadding left" >
                                    <a href="<?php the_permalink(); ?>" ><?php the_title(); ?></a>
                                </div>
                                <div class="col-xs-5 col-sm-5 col-md-5 nopadding right">

                                    <?php
                                    if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                    }
                                    else{
                                        if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                            $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                        }
                                    }
                                    if($price):
                                        $price=(float) $price;
                                        ?>
                                        <?=number_format($price,0,'.','.'); ?> đ
                                        <?php
                                    endif;
                                    ?>

                                </div>
                            </div>
                            <?php
                                endif;
                                $dem++;
                            endwhile;
                            endif;
                            wp_reset_postdata();
                            ?>


                        </div>
                    </div>


                </div>
                <?php
                endforeach;
                ?>

            </div>

        </div>

    </div>
</div>


<div class="col-xs-12 col-sm-12 nopadding"></div>


<div class="col-xs-12 col-sm-12 nopadding box-main" >
    <div class="container" >


        <div class="col-xs-12 col-sm-12 nopadding title-page">
            <i class="icon-tour"></i> <h3><?=get_cat_name($redux_theme['opt-select-cathome']); ?></h3>
        </div>


        <div class="row5">

            <div class="col-xs-12 col-sm-12 nopadding owl-carousel owl-theme owl-carousel-post">


                <?php
                $wp=new WP_Query("showposts=8&cat=".$redux_theme['opt-select-cathome']);
                if($wp->have_posts()):
                while ($wp->have_posts()):
                $wp->the_post();
                ?>
                <div class="col-xs-12 col-sm-12 nopadding item">
                    <div class="col-xs-12 col-sm-12 nopadding box-items">
                        <div class="col-xs-12 col-sm-12 nopadding text-center header-thumbnail" >
                            <a href="<?php the_permalink(); ?>" >
                                <img src="<?php
                                $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                                echo $img[0];
                                ?>" /> </a>
                        </div>
                        <div class="col-xs-12 col-sm-12 nopadding info-items-owl" >
                            <a href="<?php the_permalink(); ?>" class="link-info-items-owl" >
                                <?php the_title(); ?>
                            </a>
                            <div class="col-xs-12 col-sm-12 nopadding excerpt-items-post" >
                                <?php the_content_rss('...',0,'',50); ?>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
                endwhile;
                endif;
                wp_reset_postdata();
                ?>



            </div>

        </div>

















    </div>
</div>


<div class="col-xs-12 col-sm-12 nopadding list-severbox" >
    <div class="container" >


        <div class="col-xs-12 col-sm-12 nopadding title-page">
            <i class="icon-tour"></i> <h3>Dịch vụ du lịch</h3>
        </div>

        <div class="row">

            <div class="col-xs-12 col-sm-12 nopadding owl-carousel owl-theme owl-items-list-severbox">

                <?php
                foreach($redux_theme['opt-slides-sever'] as $key=>$value):
                ?>
                <div class="col-xs-12 col-sm-12 col-md-12 items-list-severbox" >
                    <div class="col-xs-12 col-sm-12 col-md-12 nopadding box-item-list" style="background: url('<?=$value['image']; ?>') no-repeat center;background-size: cover;" >
                        <a href="<?=$value['url']; ?>" ><?=$value['title']; ?></a>
                    </div>
                </div>
                <?php
                endforeach;
                ?>

            </div>


        </div>







    </div>
</div>

<?php get_footer(); ?>
