﻿<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
global $redux_theme;
get_header();
get_template_part("inc/menu");
get_search_form();

if(have_posts()):
while (have_posts()):
the_post();
?>

<div class="col-xs-12 col-sm-12 nopadding template-theme">
    <div class="container">
        <div class="row">

            <div class="hidden-xs hidden-sm col-md-3 sidebar">





<div class="col-xs-12 col-sm-12 col-md-12 nopadding fixscrolldk">


                <div class="col-xs-12 col-sm-12 nopadding title-sidebar-tour">
                    Thông tin Tour
                </div>
                <div class="col-xs-12 col-sm-12 nopadding info-tour-sidebar">

                    <div class="col-xs-12 col-sm-12 col-md-12 items-box-meta-tour">
                        <div class="col-xs-4 col-sm-4 col-md-4 nopadding text-left">
                            <strong>
                                Điểm đi :
                            </strong>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 nopadding text-right">
                            <strong><?php
                                $termdiemden=get_the_terms(get_the_ID(),'diemdi');
                                echo $termdiemden[0]->name;
                                ?></strong>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 items-box-meta-tour">
                        <div class="col-xs-4 col-sm-4 col-md-4 nopadding text-left">
                            <strong>
                                Điểm đến :
                            </strong>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 nopadding text-right">
                            <strong><?php
                                $termdiemden=get_the_terms(get_the_ID(),'diemden');
                                echo $termdiemden[0]->name;
                                ?></strong>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 items-box-meta-tour">
                        <div class="col-xs-4 col-sm-4 col-md-4 nopadding text-left">
                            <strong>
                                Thời gian :
                            </strong>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 nopadding text-right">
                            <?php
                            $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                            echo $meta;
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 items-box-meta-tour">
                        <div class="col-xs-4 col-sm-4 col-md-4 nopadding text-left">
                            <strong>
                                Khởi hành :
                            </strong>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 nopadding text-right">
                            <?php
                            $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                            echo $meta;
                            ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 items-box-meta-tour">
                        <div class="col-xs-4 col-sm-4 col-md-4 nopadding text-left">
                            <strong>
                                Khách sạn :
                            </strong>
                        </div>
                        <div class="col-xs-8 col-sm-8 col-md-8 nopadding text-right star-tour">
                            <?php
                            $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                            $meta=(int) $meta;
                            ?>
                            <?php
                            for($i=1;$i<=$meta;$i++):
                                ?>
                                <i class="fa fa-star"></i>
                                <?php
                            endfor;
                            ?>
                        </div>
                    </div>


                    <div class="col-xs-12 col-sm-12 nopadding pricetour">


                        <?php
                        if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                            $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                        }
                        else{
                            if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                            }
                        }
                        if($price):
                            $price=(float) $price;
                            ?>
                            <span>chỉ từ</span>
                            <?=number_format($price,0,'.','.'); ?> đ
                            <?php
                        endif;
                        ?>


                    </div>


                </div>


                <div class="col-xs-12 col-sm-12 nopadding text-center phone-contact-sidebar">

                    <span class="icon-phone-sidebar"></span>
                    <p>Gọi điện để được tư vấn</p>
                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding left">
                        <strong>Tổng đài:</strong>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 nopadding right">
                        <strong><?=$redux_theme['phonecontact']; ?></strong>
                    </div>

                </div>




</div>







            </div>

            <div class="col-xs-12 col-sm-12 col-md-9 main-box">

                <div class="col-xs-12 col-sm-12 nopadding main-box-single-tour">

                    <h1 class="title-single-tour"><?php the_title(); ?></h1>

                    <div class="col-xs-12 col-sm-12 nopadding items-loop-tour">

                        <div class="col-xs-5 col-sm-4 col-md-3 left">
                            <a href="<?php the_permalink(); ?>"><img src="<?php
                                $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                                echo $img[0];
                                ?>"> </a>
                        </div>
                        <div class="col-xs-7 col-sm-8 col-md-9 right">
                            <a href="<?php the_permalink(); ?>" class="header-title-tour"><?php the_title(); ?></a>
                            <div class="col-xs-12 col-sm-8 col-md-8 nopadding info-tour-loop">
                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Lịch trình:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour citylist">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-lichtrinh-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Thời gian:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Khởi hành:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Vận chuyển:
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-transport-tour',true);
                                        echo $meta;
                                        ?>
                                    </div>
                                </div>


                                <div class="col-xs-12 col-sm-12 nopadding">
                                    <div class="col-xs-5 col-sm-3 col-md-3 nopadding boxdiv-tour">
                                        Khách sạn :
                                    </div>
                                    <div class="col-xs-7 col-sm-9 col-md-9 nopadding boxdiv-tour box-star-tour">
                                        <?php
                                        $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                                        $meta=(int) $meta;
                                        ?>
                                        <?php
                                        for($i=1;$i<=$meta;$i++):
                                            ?>
                                            <i class="fa fa-star"></i>
                                            <?php
                                        endfor;
                                        ?>
                                    </div>
                                </div>


                            </div>

                            <div class="col-xs-12 col-sm-4 col-md-4 nopadding text-right price-box-loop">
                                <p>Giá Tour</p>

                                <?php
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                    $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                }
                                else{
                                    if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                    }
                                }
                                if($price):
                                    $price=(float) $price;
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                    ?>
                                    <p><strong><del>
                                                <?php
                                                $int=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
                                                echo number_format($int,0,'.','.');
                                                ?> đ
                                            </del></strong></p>
                                    <?php
                                        endif;
                                ?>
                                    <p class="price-tour-loop"><?=number_format($price,0,'.','.'); ?>  đ</p>
                                    <?php
                                endif;
                                ?>




                            </div>

                        </div>

                    </div>
                </div>


                <div class="col-xs-12 col-sm-12 nopadding content-tour">


<div class="col-xs-12 col-sm-12 col-md-12 bgcustomtabmobile">
<span class="showtextselect">Đặt Tour</span>
   <a href="javascript:void(0)" id="menutogglesingletour"><span class="fa fa-bars"></span></a>
</div>

                    <ul class="nav nav-tabs" role="tablist">
                        
                        <li role="presentation"  class="active"><a href="#lichtrinh" aria-controls="lichtrinh" role="tab" data-toggle="tab">
                                <span class="text">Lịch trình chi tiết</span><span class="number">2</span>
                            </a></li>
                        <li role="presentation"><a href="#hinhanh" aria-controls="hinhanh" role="tab" data-toggle="tab">
                                <span class="text"> Hình Ảnh</span><span class="number">3</span>
                            </a></li>
                        <li role="presentation"><a href="#banggia" aria-controls="banggia" role="tab" data-toggle="tab">
                                <span class="text">Bảng giá</span><span class="number">4</span>
                            </a></li>
                        <li role="presentation"><a href="#dieukhoan" aria-controls="dieukhoan" role="tab" data-toggle="tab">
                                <span class="text">Điều khoản</span><span class="number">5</span>
                            </a></li>
                        <li role="presentation"><a href="#lienhe" aria-controls="lienhe" role="tab" data-toggle="tab">
                                <span class="text">Liên hệ</span><span class="number">6</span>
                            </a></li>
<li role="presentation"><a href="#dattour" aria-controls="dattour" role="tab" data-toggle="tab">
                                <span class="text"> Đặt Tour </span><span class="number">1</span>
                            </a></li>
                    </ul>




                    <div class="col-xs-12 col-sm-12 nopadding tab-content">
                        <div role="tabpanel" class="col-xs-12 col-sm-12 nopadding tab-pane" id="dattour">

                            <div class="col-xs-12 col-sm-12 nopadding contact-tour">
                                <form class="col-xs-12 col-sm-12 nopadding form-contact-tour" method="post">
                                    <fieldset>

                                        <legend>Thông tin khách hàng :</legend>

                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Họ tên:</strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <input type="text" placeholder="Họ tên" class="form-control" id="inputusername" name="username" >
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Địa chỉ Email:</strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <input type="text" placeholder="Địa chỉ Email" id="inputuseremail" class="form-control" name="useremail" >
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Điện thoại liên hệ:</strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <input type="text" placeholder="Điện thoại liên hệ" class="form-control" id="inputphone" name="inputphone" >
                                            </div>
                                        </div>

                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Địa chỉ :</strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <input type="text" placeholder="Địa chỉ" class="form-control" id="inputaddress" name="inputaddress" >
                                            </div>
                                        </div>
                                    </fieldset>


                                    <fieldset>

                                        <legend>Thông tin Tour :</legend>

                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Tour:</strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <input type="text" placeholder="Tên Tour" value="<?php the_title(); ?>" disabled class="form-control" id="nametour" name="nametour">
                                            </div>
                                        </div>


                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Ngày khởi hành : </strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <input type="text" placeholder="Ngày khởi hành" class="form-control datepicker" id="daystart" name="daystart">
                                                <span class="iconindatepick fa fa-calendar"></span>
                                            </div>
                                        </div>


                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Tổng số khách : </strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <div class="row">

                                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                                        <span class="fontsize13"> Người lớn :</span>
                                                        <select class="form-control" id="nguoilon" name="nguoilon" >
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                                        <span class="fontsize13">  Trẻ Em :  </span>
                                                        <select class="form-control" id="treem" name="treem" >
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-4 col-sm-4 col-md-4">
                                                        <span class="fontsize13"> Em bé :</span>
                                                        <select class="form-control" id="embe" name="embe">
                                                            <option value="0">0</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-xs-12 col-sm-12 nopadding box-items-contact-form margintop15">
                                            <div class="col-xs-4 col-sm-4 col-md-4 nopadding">
                                                <strong>Yêu cầu : </strong>
                                            </div>
                                            <div class="col-xs-8 col-sm-8 col-md-8 nopadding">
                                                <textarea name="inputmessage" id="inputmessage" placeholder="Yêu cầu" class="form-control"></textarea>
                                            </div>
                                        </div>



                                    </fieldset>





                                    <fieldset>

                                        <legend>Gửi thông tin:</legend>

                                        <div class="col-xs-12 col-sm-12 nopadding text-center">
                                            <input type="submit" name="btnsubmit" id="btnsubmitform" value="Gửi thông tin đặt tour">
                                        </div>

                                        <div class="icon-loadding"></div>




                                    </fieldset>



                                </form>



                            </div>


                        </div>
                        <div role="tabpanel" class="col-xs-12 col-sm-12 nopadding tab-pane active" id="lichtrinh">
                            <?php the_content(); ?>
                        </div>
                        <div role="tabpanel" class="col-xs-12 col-sm-12 nopadding tab-pane" id="hinhanh">
                            <?php
                            $saved = get_post_meta(get_the_ID(), 'content_hinhanh', true);
                            echo showstring($saved);
                            ?>
                        </div>
                        <div role="tabpanel" class="col-xs-12 col-sm-12 nopadding tab-pane" id="banggia">
                            <?php
                            $saved = get_post_meta(get_the_ID(), 'content_banggia', true);
                            echo showstring($saved);
                            ?>
                        </div>
                        <div role="tabpanel" class="col-xs-12 col-sm-12 nopadding tab-pane" id="dieukhoan">
                            <?php
                            $saved = get_post_meta(get_the_ID(), 'content_dieukhoan', true);
                            echo showstring($saved);
                            ?>
                        </div>
                        <div role="tabpanel" class="col-xs-12 col-sm-12 nopadding tab-pane" id="lienhe">
                            <?php
                            $saved = get_post_meta(get_the_ID(), 'content_lienhe', true);
                            echo showstring($saved);
                            ?>
                        </div>
                    </div>





        <div class="col-xs-12 col-sm-12 nopadding comment-plugins">
                            
                            <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>

                            </div>




                    <div class="col-xs-12 col-sm-12 nopadding text-center phone-earphone">
                        <a href="tel:<?=$redux_theme['phonecontact']; ?>"><i class="glyphicon glyphicon-earphone"></i><?=$redux_theme['phonecontact']; ?></a>
                    </div>







                    <div class="col-xs-12 col-sm-12 nopadding nomargin template1" id="toursingletheme">



            <div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 title-single-tour">Tour Liên quan</div>

<?php 
          $termdiemden=get_the_terms(get_the_ID(),'diemden');
                               $slugdd=$termdiemden[0]->slug;
  $args = array( 'post_type' => 'tour', 'posts_per_page' => 2, 'diemden' => $slugdd);
                $wp=new WP_Query($args);
                if($wp->have_posts()):
                while ($wp->have_posts()):
                $wp->the_post();
             
?>
         <div class="col-xs-12 col-sm-6 col-md-6 items-template1 items-custom-singletour" >

                    <div class="col-xs-7 col-sm-7 col-md-7 nopadding right" >
                        <a href="<?php the_permalink(); ?>" class="link-tour" ><?php the_title(); ?></a>
                        <div class="col-xs-12 col-sm-12 nopadding info-tour" >

                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Thời gian
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-time-tour',true);
                                    echo $meta;
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Khởi hành
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-start-tour',true);
                                    echo $meta;
                                    ?>
                                </div>
                            </div>
                           
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour">
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding" >
                                    Khách sạn
                                </div>
                                <div class="col-xs-6 col-sm-6 col-md-6 nopadding">
                                    <?php
                                    $meta=get_post_meta(get_the_ID(),'meta-hotel-tour',true);
                                    $meta=(int) $meta;
                                    ?>
                                    <?php
                                    for($i=1;$i<=$meta;$i++):
                                    ?>
                                    <i class="fa fa-star"></i>
                                        <?php
                                        endfor;
                                        ?>

                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding items-info-tour tour-link-box">
                                <div class="col-xs-6 col-sm-6 col-md-12 nopadding left" >
                                    
<?php
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                                    $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                                }
                                else{
                                    if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                                        $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                                    }
                                }
                                if($price):
                                    $price=(float) $price;
                                if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)):
                                    ?>
                                    <p><strong><del>
                                                <?php
                                                $int=(float) get_post_meta(get_the_ID(),'meta-price-tour',true);
                                                echo number_format($int,0,'.','.');
                                                ?> đ
                                            </del></strong></p>
                                    <?php
                                        endif;
                                ?> Giá : <span class="price-tour"><?=number_format($price,0,'.','.'); ?> đ</span>
                                    <?php
                                endif;
                                ?>




                                </div>
                              
                            </div>

                        </div>
                    </div>



                    <div class="col-xs-5 col-sm-5 col-md-5 nopadding left" >
                        <a href="<?php the_permalink(); ?>" ><img src="<?php
                            $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                            echo $img[0];
                            ?>" /> </a>

                              <div class="col-xs-12 col-sm-12 col-md-12 nopadding right">
                                    <a href="<?php the_permalink(); ?>" class="btnreadtour" >Xem ngay</a>
                                </div>
                    </div>


                </div>

                <?php
                endwhile;
                endif;
                wp_reset_postdata();
                 ?>
                

            </div>

        </div>



                </div>





            </div>


        </div>
    </div>
</div>

<?php
endwhile;
endif;
wp_reset_query();
get_footer();
?>
