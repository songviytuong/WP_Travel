<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
include "inc/config.php";
add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      '' ),
        'menufooter1'  => __( 'Menu Footer 1', '' ),
        'menufooter2'  => __( 'Menu Footer 2', '' ),
        'menufooter3'  => __( 'Menu Footer 3', '' ),
        'menufooter4'  => __( 'Menu Footer 4', '' ),
        'menufooter5'  => __( 'Menu Footer 5', '' ),
        'menufooter6'  => __( 'Menu Footer 6', '' )
	) );


function showstring($string){
    $string=str_replace("\n","<p>",$string);
    return html_entity_decode($string);
};

	
	function itheme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget Area', '' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.', '' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'itheme_widgets_init' );


function itheme_comment_nav() {
	if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
	?>
	<nav class="navigation comment-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Comment navigation', '' ); ?></h2>
		<div class="nav-links">
			<?php
				if ( $prev_link = get_previous_comments_link( __( 'Older Comments', '' ) ) ) :
					printf( '<div class="nav-previous">%s</div>', $prev_link );
				endif;

				if ( $next_link = get_next_comments_link( __( 'Newer Comments', '' ) ) ) :
					printf( '<div class="nav-next">%s</div>', $next_link );
				endif;
			?>
		</div><!-- .nav-links -->
	</nav><!-- .comment-navigation -->
	<?php
	endif;
}



function itheme_post_thumbnail() {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
		return;
	}

	if ( is_singular() ) :
	?>

	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div>

	<?php else : ?>

	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
			the_post_thumbnail( 'post-thumbnail', array( 'alt' => get_the_title() ) );
		?>
	</a>

	<?php endif; 
}



function itheme_entry_meta() {
	if ( is_sticky() && is_home() && ! is_paged() ) {
		printf( '<span class="sticky-post">%s</span>', __( 'Featured', 'itheme' ) );
	}

	$format = get_post_format();
	if ( current_theme_supports( 'post-formats', $format ) ) {
		printf( '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
			sprintf( '<span class="screen-reader-text">%s </span>', _x( 'Format', 'Used before post format.', 'itheme' ) ),
			esc_url( get_post_format_link( $format ) ),
			get_post_format_string( $format )
		);
	}

	if ( in_array( get_post_type(), array( 'post', 'attachment' ) ) ) {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			get_the_date(),
			esc_attr( get_the_modified_date( 'c' ) ),
			get_the_modified_date()
		);

		printf( '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
			_x( 'Posted on', 'Used before publish date.', 'itheme' ),
			esc_url( get_permalink() ),
			$time_string
		);
	}

	if ( 'post' == get_post_type() ) {
		if ( is_singular() || is_multi_author() ) {
			printf( '<span class="byline"><span class="author vcard"><span class="screen-reader-text">%1$s </span><a class="url fn n" href="%2$s">%3$s</a></span></span>',
				_x( 'Author', 'Used before post author name.', 'itheme' ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_the_author()
			);
		}

		$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'itheme' ) );
		if ( $categories_list && itheme_categorized_blog() ) {
			printf( '<span class="cat-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
				_x( 'Categories', 'Used before category names.', 'itheme' ),
				$categories_list
			);
		}

		$tags_list = get_the_tag_list( '', _x( ', ', 'Used between list items, there is a space after the comma.', 'itheme' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
				_x( 'Tags', 'Used before tag names.', 'itheme' ),
				$tags_list
			);
		}
	}

	if ( is_attachment() && wp_attachment_is_image() ) {
		$metadata = wp_get_attachment_metadata();

		printf( '<span class="full-size-link"><span class="screen-reader-text">%1$s </span><a href="%2$s">%3$s &times; %4$s</a></span>',
			_x( 'Full size', 'Used before full size attachment link.', 'itheme' ),
			esc_url( wp_get_attachment_url() ),
			$metadata['width'],
			$metadata['height']
		);
	}

	if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		echo '<span class="comments-link">';
		/* translators: %s: post title */
		comments_popup_link( sprintf( __( 'Leave a comment<span class="screen-reader-text"> on %s</span>', 'itheme' ), get_the_title() ) );
		echo '</span>';
	}
}


function itheme_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'itheme_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'itheme_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so itheme_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so itheme_categorized_blog should return false.
		return false;
	}
}






















//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_topics_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x( 'Category Tour', '' ),
        'singular_name' => _x( 'Category Tour', '' ),
        'search_items' =>  __( 'Search Category Tour' ),
        'all_items' => __( 'All Category Tour' ),
        'parent_item' => __( 'Parent Category Tour' ),
        'parent_item_colon' => __( 'Parent Category Tour:' ),
        'edit_item' => __( 'Edit Category Tour' ),
        'update_item' => __( 'Update Category Tour' ),
        'add_new_item' => __( 'Add New Category Tour' ),
        'new_item_name' => __( 'New Category Tour Name' ),
        'menu_name' => __( 'Category Tour' ),
    );

// Now register the taxonomy

    register_taxonomy('category_tour',array('tour'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'category_tour' ),
    ));

}




//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_city_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_city_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x( 'Điểm đến', '' ),
        'singular_name' => _x( 'Điểm đến', '' ),
        'search_items' =>  __( 'Tìm điểm đến' ),
        'all_items' => __( 'All điểm đến' ),
        'parent_item' => __( 'Parent điểm đến' ),
        'parent_item_colon' => __( 'Parent điểm đến:' ),
        'edit_item' => __( 'Edit điểm đến' ),
        'update_item' => __( 'Update điểm đến' ),
        'add_new_item' => __( 'Add New điểm đến' ),
        'new_item_name' => __( 'New điểm đến Name' ),
        'menu_name' => __( 'điểm đến' ),
    );

// Now register the taxonomy

    register_taxonomy('diemden',array('tour'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'diemden' ),
    ));

}







//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_diemdi_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_diemdi_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x( 'Điểm Đi', '' ),
        'singular_name' => _x( 'Điểm Đi', '' ),
        'search_items' =>  __( 'Tìm điểm Đi' ),
        'all_items' => __( 'All điểm Đi' ),
        'parent_item' => __( 'Parent điểm Đi' ),
        'parent_item_colon' => __( 'Parent điểm Đi:' ),
        'edit_item' => __( 'Edit điểm Đi' ),
        'update_item' => __( 'Update điểm Đi' ),
        'add_new_item' => __( 'Add New điểm Đi' ),
        'new_item_name' => __( 'New điểm Đi Name' ),
        'menu_name' => __( 'điểm Đi' ),
    );

// Now register the taxonomy

    register_taxonomy('diemdi',array('tour'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'diemdi' ),
    ));

}








//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_tuyentour_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_tuyentour_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x( 'Tuyến Tour', '' ),
        'singular_name' => _x( 'Tuyến Tour', '' ),
        'search_items' =>  __( 'Tìm Tuyến Tour' ),
        'all_items' => __( 'All Tuyến Tour' ),
        'parent_item' => __( 'Parent Tuyến Tour' ),
        'parent_item_colon' => __( 'Parent Tuyến Tour:' ),
        'edit_item' => __( 'Edit Tuyến Tour' ),
        'update_item' => __( 'Update Tuyến Tour' ),
        'add_new_item' => __( 'Add New Tuyến Tour' ),
        'new_item_name' => __( 'New Tuyến Tour Name' ),
        'menu_name' => __( 'Tuyến Tour' ),
    );

// Now register the taxonomy

    register_taxonomy('tuyentour',array('tour'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'tuyentour' ),
    ));

}















//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_giatour_hierarchical_taxonomy', 0 );

//create a custom taxonomy name it topics for your posts

function create_giatour_hierarchical_taxonomy() {

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x( 'Giá Tour', '' ),
        'singular_name' => _x( 'Giá Tour', '' ),
        'search_items' =>  __( 'Tìm Giá Tour' ),
        'all_items' => __( 'All Giá Tour' ),
        'parent_item' => __( 'Parent Giá Tour' ),
        'parent_item_colon' => __( 'Parent Giá Tour:' ),
        'edit_item' => __( 'Edit Giá Tour' ),
        'update_item' => __( 'Update Giá Tour' ),
        'add_new_item' => __( 'Add New Giá Tour' ),
        'new_item_name' => __( 'New Giá Tour Name' ),
        'menu_name' => __( 'Giá Tour' ),
    );

// Now register the taxonomy

    register_taxonomy('giatour',array('tour'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => false,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'giatour' ),
    ));

}


function custom_post_type() {

    $labels = array(
        'name'                  => _x( 'Post Tour', 'Post Tour Name', '' ),
        'singular_name'         => _x( 'Post Tour', 'Post Tour Name', '' ),
        'menu_name'             => __( 'Post Tour', '' ),
        'name_admin_bar'        => __( 'Post Tour', '' ),
        'archives'              => __( 'Item Tour', '' ),
        'attributes'            => __( 'Item Attributes', '' ),
        'parent_item_colon'     => __( 'Parent Item:', '' ),
        'all_items'             => __( 'All Items', '' ),
        'add_new_item'          => __( 'Add New Item', '' ),
        'add_new'               => __( 'Add New', '' ),
        'new_item'              => __( 'New Item', '' ),
        'edit_item'             => __( 'Edit Item', '' ),
        'update_item'           => __( 'Update Item', '' ),
        'view_item'             => __( 'View Item', '' ),
        'view_items'            => __( 'View Items', '' ),
        'search_items'          => __( 'Search Item', '' ),
        'not_found'             => __( 'Not found', '' ),
        'not_found_in_trash'    => __( 'Not found in Trash', '' ),
        'featured_image'        => __( 'Featured Image', '' ),
        'set_featured_image'    => __( 'Set featured image', '' ),
        'remove_featured_image' => __( 'Remove featured image', '' ),
        'use_featured_image'    => __( 'Use as featured image', '' ),
        'insert_into_item'      => __( 'Insert into item', '' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', '' ),
        'items_list'            => __( 'Items list', '' ),
        'items_list_navigation' => __( 'Items list navigation', '' ),
        'filter_items_list'     => __( 'Filter items list', '' ),
    );
    $args = array(
        'label'                 => __( 'Post Tour', '' ),
        'description'           => __( 'Post Tour Description', '' ),
        'labels'                => $labels,
        'supports'              => array('title','editor','thumbnail'),
        'taxonomies'            => array( 'category_tour' ),
        'hierarchical'          => true,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 1,
        'rewrite'=>array("slug"=>"tour"),
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true
    );
    register_post_type( 'tour', $args );

}
add_action( 'init', 'custom_post_type', 0 );


















add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
    global $typenow;
    $post_type = 'tour'; // change to your post type
    $taxonomy  = 'category_tour'; // change to your taxonomy
    if ($typenow == $post_type) {
        $selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
        $info_taxonomy = get_taxonomy($taxonomy);
        wp_dropdown_categories(array(
            'show_option_all' => __("Show All {$info_taxonomy->label}"),
            'taxonomy'        => $taxonomy,
            'name'            => $taxonomy,
            'orderby'         => 'name',
            'selected'        => $selected,
            'show_count'      => true,
            'hide_empty'      => true,
        ));
    };
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
    global $pagenow;
    $post_type = 'tour'; // change to your post type
    $taxonomy  = 'category_tour'; // change to your taxonomy
    $q_vars    = &$query->query_vars;
    if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
        $term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
        $q_vars[$taxonomy] = $term->slug;
    }
}







function sm_custom_meta() {
    add_meta_box( 'sm_meta', __( 'Thông tin Tour', '' ), 'sm_meta_callback', 'tour','side' );
}
function sm_meta_callback( $post ) {
    $featured = get_post_meta( $post->ID );
    $metatimetour=get_post_meta($post->ID,'meta-time-tour',true);
    if(!$metatimetour){
        $metatimetour="";
    }
    $metastarttour=get_post_meta($post->ID,'meta-start-tour',true);
    if(!$metastarttour){
        $metastarttour="";
    }


    $metatransporttour=get_post_meta($post->ID,'meta-transport-tour',true);
    if(!$metatransporttour){
        $metatransporttour="";
    }
    $metahoteltour=get_post_meta($post->ID,'meta-hotel-tour',true);
    if(!$metahoteltour){
        $metahoteltour=1;
    }
    $metahoteltour=(int) $metahoteltour;


    $metapricetour=get_post_meta($post->ID,'meta-price-tour',true);
    if(!$metapricetour){
        $metapricetour="";
    }
    $metapricesaletour=get_post_meta($post->ID,'meta-price-sale-tour',true);
    if(!$metapricesaletour){
        $metapricesaletour="";
    }



    $metalichtrinh=get_post_meta($post->ID,'meta-lichtrinh-tour',true);
    if(!$metalichtrinh){
        $metalichtrinh="";
    }
    ?>


    <div class="sm-row-content">
        <label for="meta-hot-tour">
            <input type="checkbox" name="meta-hot-tour" id="meta-hot-tour" value="yes" <?php if ( isset ( $featured['meta-hot-tour'] ) ) checked( $featured['meta-hot-tour'][0], 'yes' ); ?> />
            <?php _e( 'Tour này có phải là tour hot ?', '' )?>
        </label>


      <div class="boxtourtheme" style="margin-top: 5px;">
          <label for="meta-time-tour">
              <input type="text" placeholder="Thời gian của Tour" name="meta-time-tour" style="width: 100%;" id="meta-time-tour" value="<?=$metatimetour; ?>" />
    <p style="margin-top: 2px;">Thời gian của Tour.</p>
    </label>
      </div>



    <div class="boxtourtheme" style="margin-top: 5px;">
        <label for="meta-start-tour">
            <input type="text" placeholder="Ngày khởi hành của Tour" name="meta-start-tour" style="width: 100%;" class="datepicker" id="meta-start-tour" value="<?=$metastarttour; ?>" />
            <p style="margin-top: 2px;">Ngày khởi hành của Tour. </p><p><em>Chọn ngày khởi hành hoặc nhập nội dung của bạn</em></p>
        </label>
    </div>



    <div class="boxtourtheme" style="margin-top: 5px;">
        <label for="meta-transport-tour">
            <input type="text" placeholder="Vận chuyển của Tour" name="meta-transport-tour" style="width: 100%;" id="meta-transport-tour" value="<?=$metatransporttour; ?>" />
            <p style="margin-top: 2px;">Vận chuyển của Tour.</p>
        </label>
    </div>




        <div class="boxtourtheme" style="margin-top: 5px;">
            <label for="meta-lichtrinh-tour">
                <input type="text" placeholder="Lịch trình của Tour" name="meta-lichtrinh-tour" style="width: 100%;" id="meta-lichtrinh-tour" value="<?=$metalichtrinh; ?>" />
                <p style="margin-top: 2px;">Lịch trình của Tour.</p>
            </label>
        </div>


        <div class="hotel" style="margin-top: 5px;">
            <label for="meta-start-tour">
                <select name="meta-hotel-tour" id="meta-hotel-tour">
                    <?php
                    for($i=1;$i<=5;$i++):
                    ?>
                    <option <?php if($metahoteltour==$i){ echo 'selected'; }; ?> value="<?=$i; ?>"><?=$i; ?> Sao</option>
                        <?php
                        endfor;
                        ?>
                </select>
                <p style="margin-top: 2px;">Khách sạn nghỉ</p>
            </label>
        </div>




        <div class="boxtourtheme" style="margin-top: 5px;">
            <label for="meta-transport-tour">
                <input type="text" placeholder="Giá tiền của Tour" name="meta-price-tour" style="width: 100%;" id="meta-price-tour" value="<?=$metapricetour; ?>" />
                <p style="margin-top: 2px;">Giá tiền của Tour.</p>
            </label>
        </div>




        <div class="boxtourtheme" style="margin-top: 5px;">
            <label for="meta-transport-tour">
                <input type="text" placeholder="Giá tiền khuyến mãi của Tour" name="meta-price-sale-tour" style="width: 100%;" id="meta-price-sale-tour" value="<?=$metapricesaletour; ?>" />
                <p style="margin-top: 2px;">Giá tiền khuyến mãi của Tour.</p>
            </label>
        </div>





    </div>

    <?php
}
add_action( 'add_meta_boxes', 'sm_custom_meta' );

function sm_meta_save( $post_id ) {
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'sm_nonce' ] ) && wp_verify_nonce( $_POST[ 'sm_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if( isset( $_POST[ 'meta-hot-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-hot-tour', 'yes' );
    } else {
        update_post_meta( $post_id, 'meta-hot-tour', '' );
    }

    if( isset( $_POST[ 'meta-time-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-time-tour', trim($_POST['meta-time-tour']) );
    }

    if( isset( $_POST[ 'meta-start-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-start-tour', trim($_POST['meta-start-tour']) );
    }

    if( isset( $_POST[ 'meta-transport-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-transport-tour', trim($_POST['meta-transport-tour']) );
    }

    if( isset( $_POST[ 'meta-hotel-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-hotel-tour',(int) $_POST['meta-hotel-tour'] );
    }

    if( isset( $_POST[ 'meta-lichtrinh-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-lichtrinh-tour',trim($_POST['meta-lichtrinh-tour']) );
    }

    if( isset( $_POST[ 'meta-price-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-price-tour',(float) $_POST['meta-price-tour'] );
    }

    if( isset( $_POST[ 'meta-price-sale-tour' ] ) ) {
        update_post_meta( $post_id, 'meta-price-sale-tour',(float) $_POST['meta-price-sale-tour'] );
    }

}
add_action( 'save_post', 'sm_meta_save' );
















function unescapez( $str )
{
    return str_replace(
        array ( '&lt;', '&gt;', '&quot;', '&amp;', '&nbsp;', '&amp;nbsp;','\&quot;','\"' )
        , array('','','','','','','','"')
        ,   $str
    );
}




add_action( 'add_meta_boxes', 'wpse_61041_add_custom_boxx' );

add_action( 'save_post', 'wpse_61041_save_postdatax' );

function wpse_61041_add_custom_boxx() {
    add_meta_box(
        'wpse_61041_sectionid',
        'Hình ảnh',
        'wpse_61041_inner_custom_boxx',
        'tour',
        'normal',
        'high'
    );
}

function wpse_61041_inner_custom_boxx($post)
{
    wp_nonce_field( 'wpse_61041_wpse_61041_field_noncex', 'wpse_61041_noncenamex' );
    $settings = array('media_buttons' => true,'textarea_rows' => '15' );

    $saved = get_post_meta( $post->ID, 'content_hinhanh', true);
    if( !$saved )
        $saved = '';
    wp_editor(html_entity_decode(unescapez($saved), ENT_QUOTES, 'UTF-8'), 'content_hinhanh', $settings);
}

function wpse_61041_save_postdatax( $post_id )
{
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    if ( isset($_POST['content_hinhanh'])){
        update_post_meta( $post_id, 'content_hinhanh',trim($_POST['content_hinhanh']) );

    }
};






















add_action( 'add_meta_boxes', 'wpse_61041_add_custom_boxx1' );

add_action( 'save_post', 'wpse_61041_save_postdatax1' );

function wpse_61041_add_custom_boxx1() {
    add_meta_box(
        'wpse_61041_sectionid1',
        'Bảng giá',
        'wpse_61041_inner_custom_boxx1',
        'tour',
        'normal',
        'high'
    );
}

function wpse_61041_inner_custom_boxx1($post)
{
    wp_nonce_field( 'wpse_61041_wpse_61041_field_noncex1', 'wpse_61041_noncenamex1' );
    $settings = array('media_buttons' => true,'textarea_rows' => '15' );

    $saved = get_post_meta( $post->ID, 'content_banggia', true);
    if( !$saved )
        $saved = '';
    wp_editor(html_entity_decode(unescapez($saved), ENT_QUOTES, 'UTF-8'), 'content_banggia', $settings);
}

function wpse_61041_save_postdatax1( $post_id )
{
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    if ( isset($_POST['content_banggia'])){
        update_post_meta( $post_id, 'content_banggia',trim($_POST['content_banggia']) );

    }
};





















add_action( 'add_meta_boxes', 'wpse_61041_add_custom_boxx2' );

add_action( 'save_post', 'wpse_61041_save_postdatax2' );

function wpse_61041_add_custom_boxx2() {
    add_meta_box(
        'wpse_61041_sectionid2',
        'Điều khoản',
        'wpse_61041_inner_custom_boxx2',
        'tour',
        'normal',
        'high'
    );
}

function wpse_61041_inner_custom_boxx2($post)
{
    wp_nonce_field( 'wpse_61041_wpse_61041_field_noncex2', 'wpse_61041_noncenamex2' );
    $settings = array('media_buttons' => true,'textarea_rows' => '15' );

    $saved = get_post_meta( $post->ID, 'content_dieukhoan', true);
    if( !$saved )
        $saved = '';
    wp_editor(html_entity_decode(unescapez($saved), ENT_QUOTES, 'UTF-8'), 'content_dieukhoan', $settings);
}

function wpse_61041_save_postdatax2( $post_id )
{
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    if ( isset($_POST['content_dieukhoan'])){
        update_post_meta( $post_id, 'content_dieukhoan',trim($_POST['content_dieukhoan']) );

    }
};
















add_action( 'add_meta_boxes', 'wpse_61041_add_custom_boxx3' );

add_action( 'save_post', 'wpse_61041_save_postdatax3' );

function wpse_61041_add_custom_boxx3() {
    add_meta_box(
        'wpse_61041_sectionid3',
        'Liên hệ',
        'wpse_61041_inner_custom_boxx3',
        'tour',
        'normal',
        'high'
    );
}

function wpse_61041_inner_custom_boxx3($post)
{
    wp_nonce_field( 'wpse_61041_wpse_61041_field_noncex3', 'wpse_61041_noncenamex3' );
    $settings = array('media_buttons' => true,'textarea_rows' => '15' );

    $saved = get_post_meta( $post->ID, 'content_lienhe', true);
    if( !$saved )
        $saved = '';
    wp_editor(html_entity_decode(unescapez($saved), ENT_QUOTES, 'UTF-8'), 'content_lienhe', $settings);
}

function wpse_61041_save_postdatax3( $post_id )
{
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    if ( isset($_POST['content_lienhe'])){
        update_post_meta( $post_id, 'content_lienhe',trim($_POST['content_lienhe']) );

    }
};


add_filter( 'wp_mail_content_type', 'wpdocs_set_html_mail_content_type' );
function wpdocs_set_html_mail_content_type() {
    return 'text/html';
}






if(!function_exists("sendformcontact")){
    function sendformcontact() {


        if(isset($_POST['nguoilon'])  && isset($_POST['treem']) && isset($_POST['inputmessage'])  && isset($_POST['embe'])  && isset($_POST['username']) && isset($_POST['useremail']) && isset($_POST['inputphone']) && isset($_POST['inputaddress']) && isset($_POST['nametour']) && isset($_POST['daystart'])){


            $nguoilon=(int) $_POST['nguoilon'];
            $treem=(int) $_POST['treem'];
            $embe=(int) $_POST['embe'];
            $inputaddress=trim(addslashes(strip_tags($_POST['inputaddress'])));
            $inputmessage=trim(addslashes(strip_tags($_POST['inputmessage'])));
            $username=trim(addslashes(strip_tags($_POST['username'])));
           $useremail=trim(addslashes(strip_tags($_POST['useremail'])));
           $inputphone=trim(addslashes(strip_tags($_POST['inputphone'])));
           $nametour=trim(addslashes(strip_tags($_POST['nametour'])));
           $daystart=trim(strip_tags($_POST['daystart']));


            $to =get_option('admin_email');
            $subject = 'Liên hệ đặt Tour '.$nametour;
            $body = '<p><strong>Họ tên : </strong>'.$username.'</p>';
            $body.='<p><strong>Địa chỉ Email : </strong>'.$useremail.'</p>';
            $body.='<p><strong>Điện thoại liên hệ: : </strong>'.$inputphone.'</p>';
            $body.='<p><strong>Địa chỉ : </strong>'.$inputaddress.'</p>';
            $body.='<p><strong>Tour : </strong>'.$nametour.'</p>';
            $body.='<p><strong>Ngày khởi hành  : </strong>'.$daystart.'</p>';
            $body.='<p><strong>Người lớn : </strong>'.$nguoilon.'</p>';
            $body.='<p><strong>Trẻ em : </strong>'.$treem.'</p>';
            $body.='<p><strong>Em bé : </strong>'.$embe.'</p>';
            $body.='<p><strong>Nội dung : </strong>'.$inputmessage.'</p>';
            wp_mail('lequoccu91@gmail.com', $subject, $body );

        }

        exit;
    }
}
add_action( 'wp_ajax_nopriv_sendformcontact', 'sendformcontact' );
add_action( 'wp_ajax_sendformcontact', 'sendformcontact' );


/*


function SearchFilter($query) {
    if ($query->is_search) {
        $query->set('post_type', 'tour');

        if(strlen($_REQUEST['daystart'])>0){

            $isValid = is_numeric(str_replace('-', '', trim(addslashes(strip_tags($_REQUEST['daystart'])))));
            if($isValid){

                $query->set('meta_query', [
                    [
                        'key' => 'meta-start-tour',
                        'value' => addslashes(trim(strip_tags($_REQUEST['daystart']))),
                        'compare' => '='
                    ]
                ]);
            }

        }

    }
    return $query;
}

add_filter('pre_get_posts','SearchFilter');
 */


























function auto_featured_image() {
    global $post;
 
    if (!has_post_thumbnail($post->ID)) {
        $attached_image = get_children( "post_parent=$post->ID&amp;post_type=attachment&amp;post_mime_type=image&amp;numberposts=1" );
         
      if ($attached_image) {
              foreach ($attached_image as $attachment_id => $attachment) {
                   set_post_thumbnail($post->ID, $attachment_id);
              }
         }
    }
}
// Use it temporary to generate all featured images
add_action('the_post', 'auto_featured_image');
// Used for new posts
add_action('save_post', 'auto_featured_image');
add_action('draft_to_publish', 'auto_featured_image');
add_action('new_to_publish', 'auto_featured_image');
add_action('pending_to_publish', 'auto_featured_image');
add_action('future_to_publish', 'auto_featured_image');
