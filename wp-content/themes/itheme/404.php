<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
get_header();
get_template_part("inc/menu");
?>
<div class="col-xs-12 col-sm-12 nopadding template-theme">
    <div class="container">
        <div class="row">

            <div class="col-xs-12 col-sm-12 col-md-12 404error text-center">
                <img src="<?=get_template_directory_uri(); ?>/img/404.png" />
            </div>

        </div>
    </div>
</div>

<?php get_footer(); ?>

