<?php

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_theme";

    // This line is only for altering the demo. Can be easily removed.
    $opt_name = apply_filters( 'redux_theme/opt_name', $opt_name );



    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => 'Options',
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Options', '' ),
        'page_title'           => __( 'Options', '' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '',
        // Page slug used to denote the panel, will be based off page title then menu title then opt_name if not provided
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
         'footer_credit'     => 'Create by Wordpress Pro',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'red',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );



    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Thiết kế WebSite WordPress Chuyên nghiệp</p>', '' ), $v );
    } else {
        $args['intro_text'] = __( '<p>Thiết kế WebSite WordPress Chuyên nghiệp</p>', '' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>Cài đặt cấu hình cho website của bạn.Ấn Save Changes để lưu lại cài đặt của bạn.</p>', '' );

    Redux::setArgs( $opt_name, $args );





Redux::setSection( $opt_name, array(
    'title'            => __( 'Cấu hình chung', '' ),
    'id'               => 'home',
    'desc'             => __( 'Cấu hình Website Chung!', '' ),
    'customizer_width' => '400px',
    'icon'             => 'el el-home',
    'fields'           => array(

        array(
            'id'       => 'opt-logo',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( 'Link Logo' ),
            'compiler' => 'true',
            'desc'     => __( 'Link Logo cho website của bạn' ),
            'subtitle' => __( 'Upload Logo cho website của bạn.' ),
        ),
          array(
            'id'       => 'opt-favicon',
            'type'     => 'media',
            'url'      => true,
            'title'    => __( 'Link Favicon' ),
            'compiler' => 'true',
            'desc'     => __( 'Link Favicon cho website của bạn' ),
            'subtitle' => __( 'Upload Favicon cho website của bạn.' ),
        ),
        array(
            'id'       => 'phonecontact',
            'type'     => 'text',
            'title'    => __( 'Phone Hotline' ),
            'subtitle' => __( 'Số điện thoại liên hệ' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'idlayerslider',
            'type'     => 'text',
            'title'    => __( 'ID LayerSlider' ),
            'subtitle' => __( 'ID WP LayerSlider Plugins' ),
            'default'  => '1',
        ),
        array(
            'id'          => 'opt-slides-user',
            'type'        => 'slides',
            'title'       => __( 'Khách hàng tiêu biểu' ),
            'subtitle'    => __( 'Liên kết website của bạn' )
        ),
        array(
            'id'          => 'opt-slides-contact',
            'type'        => 'slides',
            'title'       => __( 'Hỗ trợ tư vấn' ),
            'subtitle'    => __( 'Hỗ trợ tư vấn của bạn với khách hàng.' )
        ),

        array(
            'id'      => 'opt-editor-footer',
            'type'    => 'editor',
            'title'   => __( 'Thông tin website của bạn' ),
            'default' => 'Powered by WordPress',
            'args'    => array(
                'wpautop'       => true,
                'media_buttons' => true,
                'textarea_rows' => 7,
                'teeny'         => true,
                'quicktags'     => true,
            )
        )

        )));





Redux::setSection( $opt_name, array(
    'title' => __( 'Tùy chỉnh trang chủ', '' ),
    'id'    => 'design',
    'desc'  => __( '', '' ),
    'icon'  => 'el el-wrench',
    'fields'           => array(
        array(
            'id'       => 'opt-cattour',
            'type'     => 'select',
            'data'     => 'terms',
            'multi'    => true,
            'args'=>array('taxonomies'=>array('category_tour')),
            'title'    => __( 'Danh mục Tour hiển thị trên trang chủ', '' ),
            'subtitle' => __( 'No validation can be done on this field type', '' ),
            'desc'     => __( 'This is the description field, again good for additional info.', '' ),
        ),
        array(
            'id'       => 'opt-cattour-list',
            'type'     => 'select',
            'data'     => 'terms',
            'multi'    => true,
            'args'=>array('taxonomies'=>array('category_tour')),
            'title'    => __( 'Danh mục Tour', '' ),
            'subtitle' => __( 'No validation can be done on this field type', '' ),
            'desc'     => __( 'This is the description field, again good for additional info.', '' ),
        ),
        array(
            'id'       => 'opt-select-cathome',
            'type'     => 'select',
            'data'     => 'categories',
            'title'    => __( 'Cẩm nang du lịch' ),
            'subtitle' => __( 'Chọn danh mục cẩm nang du lịch của bạn sẽ được hiển thị trên trang chủ' ),
            'desc'     => __( '' ),
        ),
        array(
            'id'          => 'opt-slides-sever',
            'type'        => 'slides',
            'title'       => __( 'Dịch vụ du lịch' ),
            'subtitle'    => __( 'Các dịch vu du lịch của bạn.' )
        ),

    )
) );



Redux::setSection( $opt_name, array(
    'title'            =>'Liên kết mạng xã hội',
    'desc'             =>'Liên kết mạng xã hội',
    'id'               => 'basocial',
    'fields'           => array(
        array(
            'id'       => 'linkfb',
            'type'     => 'text',
            'title'    => __( 'Link Liên kết Facebook' ),
            'subtitle' => __( 'Link liên kết tới trang facebook của bạn' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'linktweeter',
            'type'     => 'text',
            'title'    => __( 'Link Liên kết Tweeter' ),
            'subtitle' => __( 'Link liên kết tới trang Tweeter của bạn' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'linkgoogle',
            'type'     => 'text',
            'title'    => __( 'Link Liên kết Google Plus' ),
            'subtitle' => __( 'Link liên kết tới trang Google Plus của bạn' ),
            'default'  => '#',
        ),


    )));