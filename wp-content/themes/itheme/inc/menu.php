<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
global $redux_theme;
?>

<div class="col-xs-12 col-sm-12 header">
    <div class="container" >
        <div class="row">
            <div class="col-xs-6 col-sm-4 logo" >
                <a href="<?=home_url(); ?>" ><img src="<?=$redux_theme['opt-logo']['url']; ?>" /> </a>
            </div>
            <div class="col-xs-6 col-sm-8 phone-contact" >

                <ul class="social">
                    <li><a href="<?=$redux_theme['linkfb']; ?>"><img src="<?=get_template_directory_uri(); ?>/img/icon-facebook.png"/> </a> </li>
                    <li><a href="<?=$redux_theme['linktweeter']; ?>"><img src="<?=get_template_directory_uri(); ?>/img/icon-tweet.png"/> </a> </li>
                    <li><a href="<?=$redux_theme['linkgoogle']; ?>"><img src="<?=get_template_directory_uri(); ?>/img/icon-youtube.png"/> </a> </li>
                </ul>
                <div class="col-xs-12 col-sm-12 nopadding text-right text-hotline" >

                    <span class="title-hotline"><img src="<?=get_template_directory_uri(); ?>/img/number-hot-line.png" /> Hotline tư vấn</span>
                    <span class="phonecontact"><?=$redux_theme['phonecontact']; ?></span>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 col-sm-12 nopadding menu-theme-wp menu" >
    <div class="container" >

        <nav id="navmenu">
            <?php

            $args = array(
                'theme_location' => 'primary',
                'container' => '',
                'before'=>'',
                'after'=>''
            );

            wp_nav_menu( $args );
            ?>

        </nav>

    </div>
</div>

<?php
if(!is_404()):
?>
<div class="col-xs-12 col-sm-12 nopadding slider" >
    <?php layerslider($redux_theme['idlayerslider']); ?>
</div>

<?php
    endif;
    ?>