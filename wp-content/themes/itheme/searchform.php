<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
?>
<div class="col-xs-12 col-sm-12 nopadding searchform" >
    <div class="container" >
        <div class="col-xs-12 col-sm-12 nopadding title-page">
            <i class="fa fa-search"></i> <h3>Tìm kiếm Tour</h3>
        </div>
        <form method="get" action="<?=home_url(); ?>">
            <div class="row5">


         <div class="col-xs-12 col-sm-9 col-md-10 padding5">

       <div class="col-xs-6 col-sm-3 col-md-3 padding5 items-form-search boxsearchto">

                    <?php
                    $args = array(
                        'show_option_all'    => '',
                        'show_option_none'   => 'Điểm đi',
                        'option_none_value'  => '0',
                        'orderby'            => 'ID',
                        'order'              => 'ASC',
                        'show_count'         => 1,
                        'hide_empty'         => 1,
                        'child_of'           => 0,
                        'exclude'            => '',
                        'include'            => '',
                        'echo'               => 1,
                        'selected'           => 0,
                        'hierarchical'       => 0,
                        'name'               => 'diemdi',
                        'id'                 => '',
                        'class'              => 'chosen-select',
                        'depth'              => 0,
                        'tab_index'          => 0,
                        'taxonomy'           => 'diemdi',
                        'hide_if_empty'      => false,
                        'value_field'	     => 'slug',
                    );
                    wp_dropdown_categories($args);
                    ?>




                </div>


                <div class="col-xs-6 col-sm-3 col-md-3 padding5 items-form-search boxsearchRoundTrip">
                    <?php
                    $args = array(
                        'show_option_all'    => '',
                        'show_option_none'   => 'Tuyến Tour',
                        'option_none_value'  => '0',
                        'orderby'            => 'ID',
                        'order'              => 'ASC',
                        'show_count'         => 1,
                        'hide_empty'         => 1,
                        'child_of'           => 0,
                        'exclude'            => '',
                        'include'            => '',
                        'echo'               => 1,
                        'selected'           => 0,
                        'hierarchical'       => 0,
                        'name'               => 'tuyentour',
                        'id'                 => '',
                        'class'              => 'chosen-select',
                        'depth'              => 0,
                        'tab_index'          => 0,
                        'taxonomy'           => 'tuyentour',
                        'hide_if_empty'      => false,
                        'value_field'	     => 'slug',
                    );
                    wp_dropdown_categories($args);
                    ?>

                </div>


                <div class="col-xs-6 col-sm-3 col-md-3 padding5 items-form-search boxsearchfrom">

                    <?php
                    $args = array(
                        'show_option_all'    => '',
                        'show_option_none'   => 'Điểm Đến',
                        'option_none_value'  => '0',
                        'orderby'            => 'ID',
                        'order'              => 'ASC',
                        'show_count'         => 1,
                        'hide_empty'         => 1,
                        'child_of'           => 0,
                        'exclude'            => '',
                        'include'            => '',
                        'echo'               => 1,
                        'selected'           => 0,
                        'hierarchical'       => 0,
                        'name'               => 'diemden',
                        'id'                 => '',
                        'class'              => 'chosen-select',
                        'depth'              => 0,
                        'tab_index'          => 0,
                        'taxonomy'           => 'diemden',
                        'hide_if_empty'      => false,
                        'value_field'	     => 'slug',
                    );
                    wp_dropdown_categories($args);
                    ?>




                </div>

 <div class="col-xs-6 col-sm-3 col-md-3 padding5 items-form-search boxsearchprice">

                    <?php
                    $args = array(
                        'show_option_all'    => '',
                        'show_option_none'   => 'Giá Tour',
                        'option_none_value'  => '0',
                        'orderby'            => 'ID',
                        'order'              => 'ASC',
                        'show_count'         => 1,
                        'hide_empty'         => 1,
                        'child_of'           => 0,
                        'exclude'            => '',
                        'include'            => '',
                        'echo'               => 1,
                        'selected'           => 0,
                        'hierarchical'       => 0,
                        'name'               => 'giatour',
                        'id'                 => '',
                        'class'              => 'chosen-select',
                        'depth'              => 0,
                        'tab_index'          => 0,
                        'taxonomy'           => 'giatour',
                        'hide_if_empty'      => false,
                        'value_field'	     => 'slug',
                    );
                    wp_dropdown_categories($args);
                    ?>

                </div>


</div>


               

                <div class="col-xs-12 col-sm-3 col-md-2 padding5 items-form-search boxsearchprice">
                    <input type="hidden" name="s" />
                    <input type="hidden" name="post_type" value="post_tour" />
                    <input type="submit" value="Tìm kiếm" id="btnsearch" />
                </div>





            </div>



        </form>
    </div>
</div>