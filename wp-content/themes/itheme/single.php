<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
global $redux_theme;
get_header();
get_template_part("inc/menu");
get_search_form();
?>
<div class="col-xs-12 col-sm-12 nopadding template-theme">
	<div class="container">
		<div class="row">

			<?php
			get_sidebar();
			?>

			<div class="col-xs-12 col-sm-12 col-md-8 main-box">




				<div class="col-xs-12 col-sm-12 col-md-12 nopadding content-post">

					<?php
					if(have_posts()):
						while (have_posts()):
							the_post();
					?>
					<h1 class="title-single-tour">
						<?php the_title(); ?>
					</h1>

					<div class="col-xs-12 col-sm-12 nopadding content-type-post">
						<?php
						the_content();
						?>
					</div>
							
							<?php
							endwhile;
							endif;
							wp_reset_query();
							?>


<div class="col-xs-12 col-sm-12 col-md-12 nopadding post-replate">

<h3 class=" text-left">
						Bài viết liên quan
					</h3>

					<div class="row">

<?php 
$all_tags = get_tags();
$tag_id = array();
foreach( $all_tags as $tag ) {
    $tag_id[] = $tag->term_id;
}
$args = array(
    'showposts' => 6,
    'tag__in' => $tag_id
);
$wp=new WP_Query($args);
if($wp->have_posts()):
	while ($wp->have_posts()):
		$wp->the_post();
?>
<div class="col-xs-6 col-sm-4 col-md-4 items-post-replate">
<div class="col-xs-12 col-sm-12 col-md-12 nopadding imgthumbnail-post">
<a href="<?php the_permalink(); ?>">
<img src="<?php
$img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
echo $img[0];
 ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
</a>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 nopadding">
<a href="<?php the_permalink(); ?>" class="linkpost" ><?php the_title(); ?></a>
</div>
</div>
<?php 
endwhile;
endif;
wp_reset_postdata();
?>


					</div>
</div>
							<div class="col-xs-12 col-sm-12 nopadding comment-plugins">
							
							<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-numposts="5"></div>

							</div>

				</div>



			</div>


		</div>
	</div>
</div>

<?php get_footer(); ?>