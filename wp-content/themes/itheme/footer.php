<?php

if ( ! defined( 'ABSPATH' ) ) {

    exit; // Exit if accessed directly

};

global $redux_theme;

?>

<div class="col-xs-12 col-sm-12 nopadding list-support" >

    <div class="container" >
        <div class="row5">
            <div class="col-xs-12 col-sm-12 nopadding owl-carousel owl-theme owl-carousel-list-support">
                <?php
                foreach ($redux_theme['opt-slides-user'] as $key=>$value):
                ?>
                <div class="col-xs-12 col-sm-12 nopadding item">
                    <div class="col-xs-12 col-sm-12 nopadding box-items">
                        <a href="<?=$value['url']; ?>" >
                            <img src="<?=$value['image']; ?>" title="<?=$value['title']; ?>" alt="<?=$value['title']; ?>" />
                        </a>
                    </div>
                </div>
                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
</div>



<div class="col-xs-12 col-sm-12 nopadding footer-support" >
    <div class="container" >
        <div class="row">
            <div class="col-xs-12 col-sm-12 nopadding owl-carousel owl-theme owl-carousel-theme">
                <?php
                foreach ($redux_theme['opt-slides-contact'] as $key=>$value):
                ?>
                <div class="col-xs-12 col-sm-12 nopadding items-support" >
                    <div class="col-xs-12 col-sm-12 nopadding box-items-support" >
                        <div class="left" >
                            <img src="<?=$value['image']; ?>" />
                        </div>
                        <div class="right" >
                            <div class="col-xs-12 col-sm-12 nopadding title-right" >
                                <a href="<?=$value['url'];?>"><?=$value['title']; ?></a>
                            </div>
                            <div class="col-xs-12 col-sm-12 nopadding info-right" >
                                <?=$value['description']; ?>
                            </div>
						</div>
                    </div>
                </div>
                <?php
                endforeach;
                ?>
            </div>
        </div>
    </div>
</div>



<div class="col-xs-12 col-sm-12 nopadding menu-footer" >
    <div class="container" >
        <div class="row" >
            <div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
               <ul>
                   <?php
                   $args = array(

                       'theme_location' => 'menufooter1',

                       'container' => '',

                       'before'=>'',

                       'after'=>'',

                       'items_wrap' => '%3$s'

                   );
                   wp_nav_menu( $args );
                   ?>
               </ul>
            </div>
			
			<div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
               <ul>
                   <p style="font-weight: bold; display: block;margin-bottom:5px;color: #666;height: 40px;overflow: hidden;font-size: 16px;"> Chấp nhận thanh toán</p>
				   <img src="http://vietkitetravel.com.vn/wp-content/uploads/2018/02/paypal.png">
				   <img src="http://vietkitetravel.com.vn/wp-content/uploads/2018/02/banking.png">
				   <img src="http://vietkitetravel.com.vn/wp-content/uploads/2018/02/momo.png">
               </ul>
            </div>
			
			<div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
               <ul>
                   <p style="font-weight: bold; display: block;margin-bottom:5px;color: #666;height: 40px;overflow: hidden;font-size: 16px;"> Quét mã QR để trò chuyện</p>
				   <img src="http://vietkitetravel.com.vn/wp-content/uploads/2018/02/FB.png">
               </ul>
            </div>
			
			<div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
               <ul>
					
                    <p style="font-weight: bold; display: block;margin-bottom:5px;color: #666;height: 40px;overflow: hidden;font-size: 16px;"> Xác nhận hoạt động </p>
				   <a rel="nofollow" href="http://online.gov.vn/HomePage/CustomWebsiteDisplay.aspx?DocId=41879" target="_blank">
				   <img src="http://vietkitetravel.com.vn/wp-content/uploads/2018/02/20150827110756-dathongbao.png" >
				   </a>
               </ul>
            </div>
         <!--   <div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
              <ul>
                  <?php
                  $args = array(

                      'theme_location' => 'menufooter2',

                      'container' => '',

                      'before'=>'',

                      'after'=>'',

                      'items_wrap' => '%3$s'
                  );
                  wp_nav_menu( $args );
                  ?>
              </ul>
            </div>
			
            <div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
                <ul>
                    <?php
                    $args = array(

                        'theme_location' => 'menufooter3',

                        'container' => '',

                        'before'=>'',

                        'after'=>'',

                        'items_wrap' => '%3$s'

                    );
                    wp_nav_menu( $args );
                    ?>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 items-menu-footer" >
               <ul>
                   <?php
                   $args = array(

                       'theme_location' => 'menufooter4',

                       'container' => '',

                       'before'=>'',

                       'after'=>'',

                       'items_wrap' => '%3$s'

                   );
                   wp_nav_menu( $args );
                   ?>
               </ul>
            </div> -->
        </div>
    </div>
</div>



<div class="col-xs-12 col-sm-12 footer" >
    <div class="container" >
        <div class="row">
            <div class="col-xs-12 col-sm-9 col-md-9 items-footer" >
                <?=showstring($redux_theme['opt-editor-footer']); ?>
            </div>
            <!--<div class="col-xs-6 col-sm-4 col-md-3 items-footer" >
                <div class="col-xs-12 col-sm-12 nopadding title-items-footer" >
                    Tin tức
                </div>

                <div class="col-xs-12 col-sm-12 menu-footer-items" >
                    <ul>
                        <?php
                        $args = array(

                            'theme_location' => 'menufooter5',

                            'container' => '',

                            'before'=>'',

                            'after'=>'',

                            'items_wrap' => '%3$s'
                        );
                        wp_nav_menu( $args );
                        ?>
                    </ul>
                </div>
            </div>



            <div class="col-xs-6 col-sm-4 col-md-3 items-footer" >
                <div class="col-xs-12 col-sm-12 nopadding title-items-footer" >
                    Góc khách hàng
                </div>

                <div class="col-xs-12 col-sm-12 menu-footer-items" >
                    <ul>
                        <?php
                        $args = array(

                            'theme_location' => 'menufooter6',

                            'container' => '',

                            'before'=>'',

                            'after'=>'',

                            'items_wrap' => '%3$s'

                        );
                        wp_nav_menu( $args );
                        ?>
                    </ul>
                </div>
            </div>
-->




            <div class="col-xs-6 col-sm-4 col-md-3 items-footer" >

                <div id="fb-root"></div>

                <script>(function(d, s, id) {

                        var js, fjs = d.getElementsByTagName(s)[0];

                        if (d.getElementById(id)) return;

                        js = d.createElement(s); js.id = id;

                        js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9";

                        fjs.parentNode.insertBefore(js, fjs);

                    }(document, 'script', 'facebook-jssdk'));</script>



                <div class="fb-page" data-href="<?=$redux_theme['linkfb']; ?>" data-tabs="timeline" data-height="240" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite=""<?=$redux_theme['linkfb']; ?>" class="fb-xfbml-parse-ignore"><a href=""<?=$redux_theme['linkfb']; ?>">Facebook</a></blockquote></div>





            </div>



        </div>



    </div>

</div>



<div id="backtotop"></div>

<?php wp_footer(); ?>


<!--Start of Tawk.to Script 

<script type="text/javascript">

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();

(function(){

var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];

s1.async=true;

s1.src='https://embed.tawk.to/58ecd5d6f7bbaa72709c5ac9/default';

s1.charset='UTF-8';

s1.setAttribute('crossorigin','*');

s0.parentNode.insertBefore(s1,s0);

})();

</script>

End of Tawk.to Script-->

<!-- Chat FB -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js#xfbml=1&version=v2.12&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
  attribution="setup_tool"
  page_id="196488164035726"
  theme_color="#fa3c4c"
  logged_in_greeting="Chào bạn ! Bạn đang quan tâm tới tour du lịch nào của VietKite Travel"
  logged_out_greeting="Chào bạn ! Bạn đang quan tâm tới tour du lịch nào của VietKite Travel">
</div>


<!-- Google Tag Manager -->

<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MXWDKP"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-MXWDKP');</script>

<!-- End Google Tag Manager -->



<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-79524951-1', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>

