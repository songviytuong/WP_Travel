<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
get_header();
get_template_part("inc/menu");
get_search_form();
?>
<div class="col-xs-12 col-sm-12 nopadding template-theme">
    <div class="container">
        <div class="row">


            <?php
            get_sidebar();
            ?>
            <div class="col-xs-12 col-sm-12 col-md-9 main-box">

<?php
the_archive_title( '<h1 class="title-block">', '</h1>' );
?>

                <?php
                the_archive_description( '<div class="col-xs-12 col-sm-12 col-md-12 nopadding taxonomy-description">', '</div>' );
                ?>
                <div class="row">

                    <?php
                    if(have_posts()):
                        while (have_posts()):
                            the_post();
                            ?>
                            <div class="col-xs-12 col-sm-12 nopadding items-loop-tour">

                                <div class="col-xs-5 col-sm-4 col-md-3 left">
                                    <a href="<?php the_permalink(); ?>"><img src="<?php
                                        $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                                        echo $img[0];
                                        ?>"> </a>
                                </div>
                                <div class="col-xs-7 col-sm-8 col-md-9 right">
                                    <a href="<?php the_permalink(); ?>" class="header-title-tour"><?php the_title(); ?></a>
                                    <div class="col-xs-12 col-sm-12 nopadding excerption-post text-justify">
                                        <?php the_content_rss('...',0,'',60); ?>
                                    </div>

                                </div>

                            </div>
                            <?php
                        endwhile;
                    endif;
                    wp_reset_query();
                    ?>

                </div>



                <div class="col-xs-12 col-sm-12 col-md-12 nopadding pagination"  >

                    <?php
                    global $wp_query;

                    $big = 999999999;

                    echo paginate_links( array(
                        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                        'format' => '?paged=%#%',
                        'prev_text'    => __('« Mới hơn'),
                        'next_text'    => __('Tiếp theo »'),
                        'current' => max( 1, get_query_var('paged') ),
                        'total' => $wp_query->max_num_pages
                    ) );
                    ?>

                </div>



            </div>


        </div>
    </div>
</div>

<?php get_footer(); ?>