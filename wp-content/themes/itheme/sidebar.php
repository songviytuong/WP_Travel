<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
?>
<div class="hidden-xs hidden-sm col-md-3 sidebar">

    <div class="col-xs-12 col-sm-12 nopadding title-sidebar">
        Tour mới
    </div>
    <div class="col-xs-12 col-sm-12 nopadding list-all-tour-hot">

        <?php
        $args = array( 'post_type' => 'tour', 'posts_per_page' =>4);
        $wp=new WP_Query($args);
        if($wp->have_posts()):
        while ($wp->have_posts()):
        $wp->the_post();
        ?>
        <div class="col-xs-12 col-sm-12 nopadding items-tour-hot">
            <div class="left">
                <a href="<?php the_permalink(); ?>"><img src="<?php
                    $img=wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'thumbnail');
                    echo $img[0];
                    ?>"> </a>
            </div>
            <div class="right">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                <div class="col-xs-12 col-sm-12 nopadding price-tour-sidebar">

                    <?php
                    if(get_post_meta(get_the_ID(),'meta-price-sale-tour',true)){
                        $price=get_post_meta(get_the_ID(),'meta-price-sale-tour',true);
                    }
                    else{
                        if(get_post_meta(get_the_ID(),'meta-price-tour',true)){
                            $price=get_post_meta(get_the_ID(),'meta-price-tour',true);
                        }
                    }
                    if($price):
                        $price=(float) $price;
                        ?>
                        Giá : <span class="price-tour"><?=number_format($price,0,'.','.'); ?> đ</span>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <?php
        endwhile;
        endif;
        wp_reset_postdata();
        ?>


    </div>


</div>
