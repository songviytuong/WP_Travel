<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
};
global $redux_theme;
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <title><?php wp_title(); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
    <link rel="shortcut icon"  href="<?=$redux_theme['opt-favicon']['url']; ?>"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/chosen/chosen.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/jquery-ui/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/owl/assets/owl.carousel.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/owl/assets/owl.theme.default.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/css/jquery-confirm.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/style.css" />
    <link rel="stylesheet" type="text/css" href="<?=get_template_directory_uri(); ?>/css/responsive.css" />
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/js/jquery.js" ></script>
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/chosen/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/owl/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/js/jquery-confirm.min.js"></script>
    <script type="text/javascript" src="<?=get_template_directory_uri(); ?>/js/site.js"></script>
	

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1078654882274918');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1078654882274918&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->







<!-- Youtube -->
<meta name="google-site-verification" content="5JxInjquthC7DDfqdW3vwpoA_unGxpTO0n7P6kejHxs" />	
<!-- End Youtube -->

</head>
<script>
fbq('track', 'Search', {
search_string: 'leather sandals'
});
</script>

<body <?php body_class(); ?>>

