jQuery(document).ready(function () {
    jQuery(".chosen-select").chosen();
    jQuery( ".datepicker" ).datepicker({
        dateFormat:'dd-mm-yy',
        minDate: 0,
    });

    jQuery('.owl-carousel-home').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
        dots:false,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            650:{
                items:3
            },
            1100:{
                items:4
            }
        }
    });




    jQuery('.owl-carousel-post').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
        dots:false,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            600:{
                items:3
            },
            1100:{
                items:4
            }
        }
    });


    jQuery('.owl-carousel-list-support').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
        dots:false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:6
            },
            1000:{
                items:10
            }
        }
    });



    jQuery('.owl-carousel-theme').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
        dots:false,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });



    jQuery('.owl-items-list-severbox').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        navText: [ '<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>' ],
        dots:false,
        responsive:{
            0:{
                items:1
            },
            480:{
                items:2
            },
            768:{
                items:3
            },
            1000:{
                items:4
            }
        }
    });


    jQuery(window).scroll(function () {

        if(jQuery(window).scrollTop()>200){

            jQuery("#backtotop").addClass("showbtt");

        }
        else{
            jQuery("#backtotop").removeClass("showbtt");
        }

    });

    jQuery("#backtotop").click(function () {

        jQuery("html,body").animate({scrollTop:0},700);

    });
    
    
    function scrollmenufix() {

        if(jQuery(window).width()>1200){

            jQuery(window).scroll(function () {
                if(jQuery(this).scrollTop()>200){

                    jQuery(".menu-theme-wp.menu").addClass("fixmenu");

                }
                else{
                    jQuery(".menu-theme-wp.menu").removeClass("fixmenu");
                }
            });

        }
        else{
            jQuery(".menu-theme-wp.menu").removeClass("fixmenu");
        }


    };

    scrollmenufix();


    function base_url() {
        var pathparts = location.pathname.split('/');
        if (location.host == 'localhost') {
            var url = location.origin+'/'+pathparts[1].trim('/')+'/';
        }else{
            var url = location.origin+"/";
        }
        return url;
    };


    jQuery("form.form-contact-tour").submit(function () {


        inputusername=jQuery("#inputusername").val();
        inputuseremail=jQuery("#inputuseremail").val();
        inputphone=jQuery("#inputphone").val();
        inputaddress=jQuery("#inputaddress").val();
        nametour=jQuery("#nametour").val();
        daystart=jQuery("#daystart").val();
        nguoilon=jQuery("#nguoilon").val();
        treem=jQuery("#treem").val();
        embe=jQuery("#embe").val();
        inputmessage=jQuery("#inputmessage").val();

        if(inputmessage.length>0 && embe.length>0 && treem.length>0 && nguoilon.length>0&&inputusername.length>0 && inputuseremail.length>0 && inputphone.length>0 && inputaddress.length>0 && nametour.length>0 && daystart.length>0){


            jQuery.ajax({
                type : 'post',
                url : base_url()+'wp-admin/admin-ajax.php',
                data : {
                    action : 'sendformcontact',
                    username:inputusername,
                    useremail:inputuseremail,
                    inputphone:inputphone,
                    inputaddress:inputaddress,
                    nametour:nametour,
                    daystart:daystart,
                    nguoilon:nguoilon,
                    treem:treem,
                    embe:embe,
                    inputmessage:inputmessage
                },
                beforeSend:function () {

                    jQuery(".icon-loadding").show();

                },
                success : function( html ) {
                    jQuery(".icon-loadding").hide();

                   jQuery("#inputusername").val("");
                   jQuery("#inputuseremail").val("");
                    jQuery("#inputphone").val("");
                    jQuery("#inputaddress").val("");
                    jQuery("#nametour").val("");
                    jQuery("#daystart").val("");
                    jQuery("#nguoilon").val(1);
                    jQuery("#treem").val("");
                    jQuery("#embe").val("");
                    jQuery("#inputmessage").val("");


                    jQuery.alert({
                        title: 'Thông báo!',
                        content: 'Yêu cầu của bạn đã được gửi tới chúng tôi.Chúng tôi sẽ liên hệ lại với bạn.!',
                    });

                }
            });


        }
        else{
            jQuery.alert({
                title: 'Thông báo!',
                content: 'Bạn vui lòng nhập đầy đủ thông tin!',
            });

        }

        return false;


    });



function scrollsidebartour(){

if(jQuery("body").hasClass('post_tour-template-default')){

    if(jQuery(window).width()>1200){

        jQuery(window).scroll(function(){


           offsetsidebartour=jQuery(".sidebar").offset().top;
          offsetendfix=jQuery("#btnsubmitform").offset().top;

          if(jQuery(this).scrollTop()>parseInt(offsetsidebartour) && jQuery(this).scrollTop()<parseInt(offsetendfix)){

            jQuery(".fixscrolldk").addClass("addscrollfixsidebartour");

          }
       if (jQuery(this).scrollTop()<offsetsidebartour) {
                jQuery(".fixscrolldk").removeClass("addscrollfixsidebartour");
          }

          if (jQuery(this).scrollTop()>offsetendfix) {
                jQuery(".fixscrolldk").removeClass("addscrollfixsidebartour");
          }
          


        });

    }
    else{
        jQuery(".fixscrolldk").removeClass("addscrollfixsidebartour");
    }


}
}

scrollsidebartour();
jQuery(window).resize(function(){
    scrollsidebartour();
});


jQuery(".bgcustomtabmobile").click(function(){

    jQuery(".content-tour .nav-tabs").toggleClass('togglemobilenavtab').toggle();


 jQuery(".content-tour .nav-tabs.togglemobilenavtab li").click(function(){
    jQuery(".content-tour .nav-tabs").hide();
    jQuery(".content-tour .nav-tabs").removeClass("togglemobilenavtab");
    jQuery(".bgcustomtabmobile .showtextselect").html(jQuery(this).find(".text").html());
 });
 
});

    
});